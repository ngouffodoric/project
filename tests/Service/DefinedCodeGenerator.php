<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Security\CodeGeneratorInterface;

class DefinedCodeGenerator implements CodeGeneratorInterface
{
    public function generate(): string
    {
        return '1234';
    }
}

