// Fichier : tests/Functional/Context/TodoList/IndexContext.php
<?php declare(strict_types=1);
namespace App\Tests\Functional\Context\TodoList;
use App\Tests\Functional\Page\TodoList\IndexPage;
use Behat\Behat\Context\Context;
class IndexContext implements Context
{
    /**
     * @var IndexPage
     */
    private $indexPage;
    public function __construct(IndexPage $indexPage)
    {
        $this->indexPage = $indexPage;
    }
    /**
     * @Then je dois être sur la liste des todo listes
     */
    public function jeDoisEtreSurLaListeDesTodoListes()
    {
        $this->indexPage->verify();
    }
}
